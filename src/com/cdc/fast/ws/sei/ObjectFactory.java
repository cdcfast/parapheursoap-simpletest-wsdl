
package com.cdc.fast.ws.sei;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.cdc.fast.ws.sei package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DownloadResponse_QNAME = new QName("http://sei.ws.fast.cdc.com/", "downloadResponse");
    private final static QName _Rawdelete_QNAME = new QName("http://sei.ws.fast.cdc.com/", "rawdelete");
    private final static QName _Download_QNAME = new QName("http://sei.ws.fast.cdc.com/", "download");
    private final static QName _HistoryResponse_QNAME = new QName("http://sei.ws.fast.cdc.com/", "historyResponse");
    private final static QName _Upload_QNAME = new QName("http://sei.ws.fast.cdc.com/", "upload");
    private final static QName _HistoryEntryVO_QNAME = new QName("http://sei.ws.fast.cdc.com/", "HistoryEntryVO");
    private final static QName _ListRemainingAcknowledgementsResponse_QNAME = new QName("http://sei.ws.fast.cdc.com/", "listRemainingAcknowledgementsResponse");
    private final static QName _DocumentContentVO_QNAME = new QName("http://sei.ws.fast.cdc.com/", "DocumentContentVO");
    private final static QName _UploadMetaResponse_QNAME = new QName("http://sei.ws.fast.cdc.com/", "uploadMetaResponse");
    private final static QName _Delete_QNAME = new QName("http://sei.ws.fast.cdc.com/", "delete");
    private final static QName _DownloadAcknowledgementResponse_QNAME = new QName("http://sei.ws.fast.cdc.com/", "downloadAcknowledgementResponse");
    private final static QName _UploadResponse_QNAME = new QName("http://sei.ws.fast.cdc.com/", "uploadResponse");
    private final static QName _RefuseSMSResponse_QNAME = new QName("http://sei.ws.fast.cdc.com/", "refuseSMSResponse");
    private final static QName _RawdeleteResponse_QNAME = new QName("http://sei.ws.fast.cdc.com/", "rawdeleteResponse");
    private final static QName _UploadMeta_QNAME = new QName("http://sei.ws.fast.cdc.com/", "uploadMeta");
    private final static QName _ValidateSMS_QNAME = new QName("http://sei.ws.fast.cdc.com/", "validateSMS");
    private final static QName _DeleteResponse_QNAME = new QName("http://sei.ws.fast.cdc.com/", "deleteResponse");
    private final static QName _History_QNAME = new QName("http://sei.ws.fast.cdc.com/", "history");
    private final static QName _RefuseSMS_QNAME = new QName("http://sei.ws.fast.cdc.com/", "refuseSMS");
    private final static QName _ListRemainingAcknowledgements_QNAME = new QName("http://sei.ws.fast.cdc.com/", "listRemainingAcknowledgements");
    private final static QName _ValidateSMSResponse_QNAME = new QName("http://sei.ws.fast.cdc.com/", "validateSMSResponse");
    private final static QName _DownloadAcknowledgement_QNAME = new QName("http://sei.ws.fast.cdc.com/", "downloadAcknowledgement");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.cdc.fast.ws.sei
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DocumentContentVO }
     * 
     */
    public DocumentContentVO createDocumentContentVO() {
        return new DocumentContentVO();
    }

    /**
     * Create an instance of {@link ListRemainingAcknowledgementsResponse }
     * 
     */
    public ListRemainingAcknowledgementsResponse createListRemainingAcknowledgementsResponse() {
        return new ListRemainingAcknowledgementsResponse();
    }

    /**
     * Create an instance of {@link Download }
     * 
     */
    public Download createDownload() {
        return new Download();
    }

    /**
     * Create an instance of {@link DownloadResponse }
     * 
     */
    public DownloadResponse createDownloadResponse() {
        return new DownloadResponse();
    }

    /**
     * Create an instance of {@link Rawdelete }
     * 
     */
    public Rawdelete createRawdelete() {
        return new Rawdelete();
    }

    /**
     * Create an instance of {@link HistoryResponse }
     * 
     */
    public HistoryResponse createHistoryResponse() {
        return new HistoryResponse();
    }

    /**
     * Create an instance of {@link Upload }
     * 
     */
    public Upload createUpload() {
        return new Upload();
    }

    /**
     * Create an instance of {@link HistoryEntryVO }
     * 
     */
    public HistoryEntryVO createHistoryEntryVO() {
        return new HistoryEntryVO();
    }

    /**
     * Create an instance of {@link ValidateSMS }
     * 
     */
    public ValidateSMS createValidateSMS() {
        return new ValidateSMS();
    }

    /**
     * Create an instance of {@link DeleteResponse }
     * 
     */
    public DeleteResponse createDeleteResponse() {
        return new DeleteResponse();
    }

    /**
     * Create an instance of {@link History }
     * 
     */
    public History createHistory() {
        return new History();
    }

    /**
     * Create an instance of {@link UploadMeta }
     * 
     */
    public UploadMeta createUploadMeta() {
        return new UploadMeta();
    }

    /**
     * Create an instance of {@link RefuseSMS }
     * 
     */
    public RefuseSMS createRefuseSMS() {
        return new RefuseSMS();
    }

    /**
     * Create an instance of {@link ListRemainingAcknowledgements }
     * 
     */
    public ListRemainingAcknowledgements createListRemainingAcknowledgements() {
        return new ListRemainingAcknowledgements();
    }

    /**
     * Create an instance of {@link DownloadAcknowledgement }
     * 
     */
    public DownloadAcknowledgement createDownloadAcknowledgement() {
        return new DownloadAcknowledgement();
    }

    /**
     * Create an instance of {@link ValidateSMSResponse }
     * 
     */
    public ValidateSMSResponse createValidateSMSResponse() {
        return new ValidateSMSResponse();
    }

    /**
     * Create an instance of {@link Delete }
     * 
     */
    public Delete createDelete() {
        return new Delete();
    }

    /**
     * Create an instance of {@link UploadMetaResponse }
     * 
     */
    public UploadMetaResponse createUploadMetaResponse() {
        return new UploadMetaResponse();
    }

    /**
     * Create an instance of {@link UploadResponse }
     * 
     */
    public UploadResponse createUploadResponse() {
        return new UploadResponse();
    }

    /**
     * Create an instance of {@link DownloadAcknowledgementResponse }
     * 
     */
    public DownloadAcknowledgementResponse createDownloadAcknowledgementResponse() {
        return new DownloadAcknowledgementResponse();
    }

    /**
     * Create an instance of {@link RefuseSMSResponse }
     * 
     */
    public RefuseSMSResponse createRefuseSMSResponse() {
        return new RefuseSMSResponse();
    }

    /**
     * Create an instance of {@link RawdeleteResponse }
     * 
     */
    public RawdeleteResponse createRawdeleteResponse() {
        return new RawdeleteResponse();
    }

    /**
     * Create an instance of {@link DataFileVO }
     * 
     */
    public DataFileVO createDataFileVO() {
        return new DataFileVO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DownloadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "downloadResponse")
    public JAXBElement<DownloadResponse> createDownloadResponse(DownloadResponse value) {
        return new JAXBElement<DownloadResponse>(_DownloadResponse_QNAME, DownloadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Rawdelete }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "rawdelete")
    public JAXBElement<Rawdelete> createRawdelete(Rawdelete value) {
        return new JAXBElement<Rawdelete>(_Rawdelete_QNAME, Rawdelete.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Download }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "download")
    public JAXBElement<Download> createDownload(Download value) {
        return new JAXBElement<Download>(_Download_QNAME, Download.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HistoryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "historyResponse")
    public JAXBElement<HistoryResponse> createHistoryResponse(HistoryResponse value) {
        return new JAXBElement<HistoryResponse>(_HistoryResponse_QNAME, HistoryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Upload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "upload")
    public JAXBElement<Upload> createUpload(Upload value) {
        return new JAXBElement<Upload>(_Upload_QNAME, Upload.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HistoryEntryVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "HistoryEntryVO")
    public JAXBElement<HistoryEntryVO> createHistoryEntryVO(HistoryEntryVO value) {
        return new JAXBElement<HistoryEntryVO>(_HistoryEntryVO_QNAME, HistoryEntryVO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListRemainingAcknowledgementsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "listRemainingAcknowledgementsResponse")
    public JAXBElement<ListRemainingAcknowledgementsResponse> createListRemainingAcknowledgementsResponse(ListRemainingAcknowledgementsResponse value) {
        return new JAXBElement<ListRemainingAcknowledgementsResponse>(_ListRemainingAcknowledgementsResponse_QNAME, ListRemainingAcknowledgementsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentContentVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "DocumentContentVO")
    public JAXBElement<DocumentContentVO> createDocumentContentVO(DocumentContentVO value) {
        return new JAXBElement<DocumentContentVO>(_DocumentContentVO_QNAME, DocumentContentVO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UploadMetaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "uploadMetaResponse")
    public JAXBElement<UploadMetaResponse> createUploadMetaResponse(UploadMetaResponse value) {
        return new JAXBElement<UploadMetaResponse>(_UploadMetaResponse_QNAME, UploadMetaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Delete }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "delete")
    public JAXBElement<Delete> createDelete(Delete value) {
        return new JAXBElement<Delete>(_Delete_QNAME, Delete.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DownloadAcknowledgementResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "downloadAcknowledgementResponse")
    public JAXBElement<DownloadAcknowledgementResponse> createDownloadAcknowledgementResponse(DownloadAcknowledgementResponse value) {
        return new JAXBElement<DownloadAcknowledgementResponse>(_DownloadAcknowledgementResponse_QNAME, DownloadAcknowledgementResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UploadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "uploadResponse")
    public JAXBElement<UploadResponse> createUploadResponse(UploadResponse value) {
        return new JAXBElement<UploadResponse>(_UploadResponse_QNAME, UploadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RefuseSMSResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "refuseSMSResponse")
    public JAXBElement<RefuseSMSResponse> createRefuseSMSResponse(RefuseSMSResponse value) {
        return new JAXBElement<RefuseSMSResponse>(_RefuseSMSResponse_QNAME, RefuseSMSResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RawdeleteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "rawdeleteResponse")
    public JAXBElement<RawdeleteResponse> createRawdeleteResponse(RawdeleteResponse value) {
        return new JAXBElement<RawdeleteResponse>(_RawdeleteResponse_QNAME, RawdeleteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UploadMeta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "uploadMeta")
    public JAXBElement<UploadMeta> createUploadMeta(UploadMeta value) {
        return new JAXBElement<UploadMeta>(_UploadMeta_QNAME, UploadMeta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateSMS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "validateSMS")
    public JAXBElement<ValidateSMS> createValidateSMS(ValidateSMS value) {
        return new JAXBElement<ValidateSMS>(_ValidateSMS_QNAME, ValidateSMS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "deleteResponse")
    public JAXBElement<DeleteResponse> createDeleteResponse(DeleteResponse value) {
        return new JAXBElement<DeleteResponse>(_DeleteResponse_QNAME, DeleteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link History }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "history")
    public JAXBElement<History> createHistory(History value) {
        return new JAXBElement<History>(_History_QNAME, History.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RefuseSMS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "refuseSMS")
    public JAXBElement<RefuseSMS> createRefuseSMS(RefuseSMS value) {
        return new JAXBElement<RefuseSMS>(_RefuseSMS_QNAME, RefuseSMS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListRemainingAcknowledgements }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "listRemainingAcknowledgements")
    public JAXBElement<ListRemainingAcknowledgements> createListRemainingAcknowledgements(ListRemainingAcknowledgements value) {
        return new JAXBElement<ListRemainingAcknowledgements>(_ListRemainingAcknowledgements_QNAME, ListRemainingAcknowledgements.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateSMSResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "validateSMSResponse")
    public JAXBElement<ValidateSMSResponse> createValidateSMSResponse(ValidateSMSResponse value) {
        return new JAXBElement<ValidateSMSResponse>(_ValidateSMSResponse_QNAME, ValidateSMSResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DownloadAcknowledgement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.ws.fast.cdc.com/", name = "downloadAcknowledgement")
    public JAXBElement<DownloadAcknowledgement> createDownloadAcknowledgement(DownloadAcknowledgement value) {
        return new JAXBElement<DownloadAcknowledgement>(_DownloadAcknowledgement_QNAME, DownloadAcknowledgement.class, null, value);
    }

}
