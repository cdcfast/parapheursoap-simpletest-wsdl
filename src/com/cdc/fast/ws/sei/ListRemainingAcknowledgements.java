
package com.cdc.fast.ws.sei;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour listRemainingAcknowledgements complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="listRemainingAcknowledgements">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="siren" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listRemainingAcknowledgements", propOrder = {
    "siren"
})
public class ListRemainingAcknowledgements {

    protected String siren;

    /**
     * Obtient la valeur de la propri�t� siren.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiren() {
        return siren;
    }

    /**
     * D�finit la valeur de la propri�t� siren.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiren(String value) {
        this.siren = value;
    }

}
