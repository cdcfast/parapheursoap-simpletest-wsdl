
package com.cdc.fast.ws.sei;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import documenttarget.CXFDocumentServiceService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 2.7.5
 * 2014-10-24T11:21:42.489+02:00
 * Generated source version: 2.7.5
 * 
 */
public final class DocumentService_DocumentPort_Client {

    private static final QName SERVICE_NAME = new QName("documentTarget", "CXFDocumentServiceService");

    private DocumentService_DocumentPort_Client() {
    }

    public static void main(String args[]) throws java.lang.Exception {
    	
    	System.setProperty("javax.net.debug", "ssl"); // see debug in logs
    	
    	//pour configurer le proxy
    	/*System.setProperty( "https.proxyHost", "proxyserver" );
    	System.setProperty( "https.proxyPort", "8080" );*/

    	
        URL wsdlURL = CXFDocumentServiceService.WSDL_LOCATION;
        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
            File wsdlFile = new File(args[0]);
            try {
                if (wsdlFile.exists()) {
                    wsdlURL = wsdlFile.toURI().toURL();
                } else {
                    wsdlURL = new URL(args[0]);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        
        
        CXFDocumentServiceService ss = new CXFDocumentServiceService(wsdlURL, SERVICE_NAME);
        DocumentService port = ss.getDocumentPort();  
        
        //on defini les certificats a utiliser pour communication SSL
        System.setProperty("javax.net.ssl.trustStore", "C:\\Program Files\\Java\\jdk1.7.0_51\\jre\\lib\\security\\cacerts");
        System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
        System.setProperty("javax.net.ssl.keyStore", "C:\\fastUtilisateursignataireMaville.p12");
        System.setProperty("javax.net.ssl.keyStorePassword", "fastpwd");
        System.setProperty("javax.net.ssl.trustStoreType", "JKS");
        System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");
        
        {
        System.out.println("Invoking download...");
        java.lang.String _download_documentId = "1966c53b-c401-4577-a750-eddda8cffd6e";
        com.cdc.fast.ws.sei.DocumentContentVO _download__return = port.download(_download_documentId);
        System.out.println("download.result=" + _download__return);

        //pour ecrire le contenu dans un fichier a la racine du projet
        InputStream in = _download__return.getContent().getInputStream();
		OutputStream outputStream = new FileOutputStream(new File("testout.pdf"));
		int read = 0;
		byte[] bytes = new byte[1024];
 
		while ((read = in.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}
		//fichier ecrit !

        }
      /*  {
        System.out.println("Invoking rawdelete...");
        java.lang.String _rawdelete_documentId = "";
        port.rawdelete(_rawdelete_documentId);


        }
        {
        System.out.println("Invoking uploadMeta...");
        java.lang.String _uploadMeta_documentId = "";
        byte[] _uploadMeta_arg1 = new byte[0];
        java.lang.String _uploadMeta__return = port.uploadMeta(_uploadMeta_documentId, _uploadMeta_arg1);
        System.out.println("uploadMeta.result=" + _uploadMeta__return);


        }
        {
        System.out.println("Invoking downloadAcknowledgement...");
        java.lang.String _downloadAcknowledgement_documentId = "";
        com.cdc.fast.ws.sei.DocumentContentVO _downloadAcknowledgement__return = port.downloadAcknowledgement(_downloadAcknowledgement_documentId);
        System.out.println("downloadAcknowledgement.result=" + _downloadAcknowledgement__return);


        }
        {
        System.out.println("Invoking upload...");
        java.lang.String _upload_label = "";
        java.lang.String _upload_comment = "";
        java.lang.String _upload_subscriberId = "";
        java.lang.String _upload_circuitId = "";
        com.cdc.fast.ws.sei.DataFileVO _upload_dataFileVO = null;
        java.lang.String _upload__return = port.upload(_upload_label, _upload_comment, _upload_subscriberId, _upload_circuitId, _upload_dataFileVO);
        System.out.println("upload.result=" + _upload__return);


        }
        {
        System.out.println("Invoking delete...");
        java.lang.String _delete_documentId = "";
        port.delete(_delete_documentId);


        }
        {
        System.out.println("Invoking listRemainingAcknowledgements...");
        java.lang.String _listRemainingAcknowledgements_siren = "";
        java.util.List<java.lang.String> _listRemainingAcknowledgements__return = port.listRemainingAcknowledgements(_listRemainingAcknowledgements_siren);
        System.out.println("listRemainingAcknowledgements.result=" + _listRemainingAcknowledgements__return);


        }
        {
        System.out.println("Invoking validateSMS...");
        java.lang.String _validateSMS_documentId = "";
        java.lang.String _validateSMS_pin = "";
        int _validateSMS__return = port.validateSMS(_validateSMS_documentId, _validateSMS_pin);
        System.out.println("validateSMS.result=" + _validateSMS__return);


        }
        {
        System.out.println("Invoking history...");
        java.lang.String _history_documentId = "";
        java.util.List<com.cdc.fast.ws.sei.HistoryEntryVO> _history__return = port.history(_history_documentId);
        System.out.println("history.result=" + _history__return);


        }
        {
        System.out.println("Invoking refuseSMS...");
        java.lang.String _refuseSMS_documentId = "";
        port.refuseSMS(_refuseSMS_documentId);


        }*/

        System.exit(0);
    }

}
